package Dresses;

import ChromeDriverTest.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CasualDresses extends Page {

    public CasualDresses(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "(//a[contains(text(),'Dresses')])[5]")
    private WebElement dresses;

    @FindBy(xpath = "(//a[contains(text(),'Casual Dresses')])[3]")
    private WebElement casualDresses;

    @FindBy(xpath = "//img[@alt='Printed Dress']")
    private WebElement skirt;

    @FindBy(xpath = "//span[contains(.,'Add to cart')]")
    private WebElement addToCardButton;


    public void clickDressesCategory() {
        getWaitDriver().until(ExpectedConditions.visibilityOf(this.dresses));
        this.dresses.click();
    }

    public void clickCasualDressesCategory() {
        getWaitDriver().until(ExpectedConditions.visibilityOf(this.casualDresses));
        this.casualDresses.click();
    }

    public void addSkirtToCart() {
        getWaitDriver().until(ExpectedConditions.visibilityOf(this.skirt));
        Actions actions = new Actions(getDriver());
        actions.moveToElement(skirt);
        actions.moveToElement(addToCardButton);
        actions.click().build().perform();
    }
}