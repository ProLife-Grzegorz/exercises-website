package Tests;

import ChromeDriverTest.ChromeDriverTest;
import Dresses.CasualDresses;
import org.junit.Assert;
import org.junit.Test;

public class Scenario3Test extends ChromeDriverTest {

    private static String URL = "http://automationpractice.com/index.php";
    private String addedToCart = "Product successfully added to your shopping cart 2";

    @Test
    public void dressesTest() {
        getDriver().get(URL);
        CasualDresses casualDresses = new CasualDresses(getDriver());
        casualDresses.clickDressesCategory();
        casualDresses.clickCasualDressesCategory();
        casualDresses.addSkirtToCart();
        Assert.assertEquals(addedToCart, "Product successfully added to your shopping cart 2");
    }
}