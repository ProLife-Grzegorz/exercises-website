package Tests;

import Authorization.LoginForm;
import ChromeDriverTest.ChromeDriverTest;
import org.junit.Assert;
import org.junit.Test;

public class Scenario2Test extends ChromeDriverTest {

    private static String URL = "http://automationpractice.com/index.php";

    @Test
    public void loginTest() {
        getDriver().get(URL);
        LoginForm loginForm = new LoginForm(getDriver());
        loginForm.clickSignIn();
        String URL = getDriver().getCurrentUrl();
        Assert.assertEquals("http://automationpractice.com/index.php?controller=authentication&back=my-account", URL);
        loginForm.login();
    }
}
