package Tests;

import Authorization.RegistrationForm;
import ChromeDriverTest.ChromeDriverTest;
import org.junit.Assert;
import org.junit.Test;

public class Scenario1Test extends ChromeDriverTest {
    private static String URL = "http://automationpractice.com/index.php";
    private String invalidEmailMessage = "Invalid email address";
    private String correctEmail = "Your personal information";
    private String myAccount = "MY ACCOUNT";


    @Test
    public void signInPageTest() {
        getDriver().get(URL);
        RegistrationForm registrationForm = new RegistrationForm(getDriver());
        registrationForm.clickSignIn();
        String URL = getDriver().getCurrentUrl();
        Assert.assertEquals("http://automationpractice.com/index.php?controller=authentication&back=my-account", URL);
    }

    @Test
    public void emptyAccountCreationTest() {
        getDriver().get(URL);
        RegistrationForm registrationForm = new RegistrationForm(getDriver());
        registrationForm.clickSignIn();
        String URL = getDriver().getCurrentUrl();
        Assert.assertEquals("http://automationpractice.com/index.php?controller=authentication&back=my-account", URL);
        registrationForm.clickCreateAccountButton();
        Assert.assertEquals(invalidEmailMessage, "Invalid email address");
    }

    @Test
    public void failAccountCreationTest() {
        getDriver().get(URL);
        RegistrationForm registrationForm = new RegistrationForm(getDriver());
        registrationForm.clickSignIn();
        String URL = getDriver().getCurrentUrl();
        Assert.assertEquals("http://automationpractice.com/index.php?controller=authentication&back=my-account", URL);
        registrationForm.setWrongEmail();
        Assert.assertEquals(invalidEmailMessage, "Invalid email address");
    }

    @Test
    public void CorrectAccountCreationTest() {
        getDriver().get(URL);
        RegistrationForm registrationForm = new RegistrationForm(getDriver());
        registrationForm.clickSignIn();
        String URL = getDriver().getCurrentUrl();
        Assert.assertEquals("http://automationpractice.com/index.php?controller=authentication&back=my-account", URL);
        registrationForm.setCorrectEmail();
        Assert.assertEquals(correctEmail, "Your personal information");
    }

    @Test
    public void FillingCreationAccountTest() {
        getDriver().get(URL);
        RegistrationForm registrationForm = new RegistrationForm(getDriver());
        registrationForm.clickSignIn();
        String URL = getDriver().getCurrentUrl();
        Assert.assertEquals("http://automationpractice.com/index.php?controller=authentication&back=my-account", URL);
        registrationForm.setCorrectEmail();
        Assert.assertEquals(correctEmail, "Your personal information");
        registrationForm.fillTheForm();
        Assert.assertEquals(correctEmail, "Your personal information");
        registrationForm.clickRegisterButton();
    }
}
