package ChromeDriverTest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class ChromeDriverTest {
    private static WebDriver driver;

    @BeforeClass
    public static void baseInitialization() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @After
    public void deleteCookies() {
        driver.manage().deleteAllCookies();
    }

    @AfterClass
    public static void closeWindow() {
        driver.close();
    }

    protected WebDriver getDriver() {
        return driver;
    }
}
