package ChromeDriverTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {

    private WebDriver driver;
    private WebDriverWait waitDriver;

    public Page(WebDriver driver) {
        this.driver = driver;
        waitDriver = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebDriverWait getWaitDriver() {
        return waitDriver;
    }
}
