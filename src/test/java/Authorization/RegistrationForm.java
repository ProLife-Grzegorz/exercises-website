package Authorization;

import ChromeDriverTest.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;


public class RegistrationForm extends Page {
    @FindBy(css = ".login")
    private WebElement sigIn;

    @FindBy (xpath = "//button[@id='SubmitCreate']/span")
    private WebElement accountButton;

    @FindBy (xpath = "//input[@id='email_create']")
    private WebElement email;

    @FindBy (id = "customer_firstname")
    private WebElement customerFirstName;

    @FindBy (id = "customer_lastname")
    private WebElement customerLastName;

    @FindBy (id = "passwd")
    private WebElement password;

    @FindBy (id = "firstname")
    private WebElement firstName;

    @FindBy (id = "lastname")
    private WebElement lastName;

    @FindBy (id = "address1")
    private WebElement address;

    @FindBy (id = "city")
    private WebElement city;

    @FindBy (id = "id_state")
    private WebElement state;

    @FindBy (id = "postcode")
    private WebElement postcode;

    @FindBy (id = "phone_mobile")
    private WebElement mobilePhone;

    @FindBy (id = "alias")
    private WebElement alias;

    @FindBy (xpath = "//button[@id='submitAccount']/span")
    private WebElement registerButton;

    public RegistrationForm(WebDriver driver) {
        super(driver);
    }


    public void clickSignIn() {
        getWaitDriver().until(ExpectedConditions.visibilityOf(this.sigIn));
        this.sigIn.click();
    }

    public void clickCreateAccountButton() {
        getWaitDriver().until(ExpectedConditions.visibilityOf(this.accountButton));
        this.accountButton.click();
    }

    public void setWrongEmail() {
        getWaitDriver().until(ExpectedConditions.visibilityOf(this.email));
        this.email.sendKeys("Test");
        this.accountButton.click();
    }

    public void setCorrectEmail() {
        getWaitDriver().until(ExpectedConditions.visibilityOf(this.email));
        this.email.sendKeys("alamakota@kotmaale5.com");
        this.accountButton.click();
    }

    public void fillTheForm() {
        getWaitDriver().until(ExpectedConditions.visibilityOf(this.firstName));
        this.customerFirstName.sendKeys("CustomerFirstName");
        this.customerLastName.sendKeys("CustomerLastName");
        this.password.sendKeys("Five12345");
        this.firstName.sendKeys("FirstName");
        this.lastName.sendKeys("LastName");
        this.address.sendKeys("Poland");
        this.city.sendKeys("Warszawa");
        Select dropdown = new Select(state);
        dropdown.selectByVisibleText("Alabama");
        this.postcode.sendKeys("12345");
        this.mobilePhone.sendKeys("1231231312");
        this.alias.sendKeys("123123");
    }

    public void clickRegisterButton() {
        getWaitDriver().until(ExpectedConditions.visibilityOf(this.registerButton));
        this.registerButton.click();
    }
}
