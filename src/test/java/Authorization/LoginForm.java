package Authorization;

import ChromeDriverTest.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginForm extends Page {

    public LoginForm(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".login")
    private WebElement sigIn;

    @FindBy(id = "email")
    private WebElement email;

    @FindBy(id = "passwd")
    private WebElement password;

    @FindBy(xpath = "//button[@id='SubmitLogin']/span")
    private WebElement loginButton;


    public void clickSignIn() {
        getWaitDriver().until(ExpectedConditions.visibilityOf(this.sigIn));
        this.sigIn.click();
    }

    public void login() {
        getWaitDriver().until(ExpectedConditions.visibilityOf(this.email));
        this.email.sendKeys("alamakota@kotmaale.com");
        this.password.sendKeys("Five12345");
        this.loginButton.click();
    }
}
